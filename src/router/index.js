/**
 * Created by whlee21 on 17. 7. 6.
 */

import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from 'src/components/Home/home';
import Posts from 'src/components/Posts/posts';
import Post from 'src/components/Posts/post';
import CreatePost from 'src/components/Posts/createPost';
import EditPost from 'src/components/Posts/editPost';
import Login from 'src/components/Login/login';
import NotFound from 'src/components/NotFound/notFound';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/posts',
    component: Posts
  },
  {
    path: '/posts/create',
    name: 'createPost',
    component: CreatePost
  },
  {
    path: '/post/:id',
    name: 'post',
    component: Post
  },
  {
    path: '/post/:id/edit',
    name: 'editPost',
    component: EditPost
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '*',
    component: NotFound
  }
];

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active',
});

router.beforeEach((to, from, next) => {
  if (to.path !== '/login') {
    var storeStr = localStorage.getItem('vuex');
    var store = JSON.parse(storeStr);
    if (store.isLoggedIn) {
      console.log('There is a token, resume(' + to.path + ')');
      next();
    } else {
      console.log('There is no token, redirect to login. (' + to.path + ')');
      next('login');
    }
  } else {
    console.log('Your\'re on the login page');
    next();
  }
});

export default router;
