/**
 * Created by whlee21 on 17. 7. 5.
 */

import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import router from 'src/router';

const LOGIN = 'LOGIN';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGOUT = 'LOGOUT';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    isLoggedIn: localStorage.getItem('token'),
  },
  plugins: [createPersistedState()],
  mutations: {
    [LOGIN](state) {
      state.pending = true;
    },
    [LOGIN_SUCCESS](state) {
      state.isLoggedIn = true;
      state.pending = false;
    },
    [LOGOUT](state) {
      state.isLoggedIn = false;
    }
  },
  actions: {
    login({
            state,
            commit,
            rootState
          }, creds) {
      console.log('login...', creds);
      commit(LOGIN); // show spinner
      return new Promise(resolve => {
        setTimeout(() => {
          localStorage.setItem('token', 'JWT');
          commit(LOGIN_SUCCESS);
          resolve();
        }, 1000);
      });
    },
    logout({
             commit
           }) {
      localStorage.removeItem('token');
      router.push('/login');
      commit(LOGOUT);
    }
  },
  getters: {
    isLoggedIn: state => {
      return state.isLoggedIn;
    }
  }
});

export default store;
