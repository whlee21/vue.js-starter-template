import Vue from 'vue';
import VueRouter from 'vue-router';
import { sync } from 'vuex-router-sync';
import VeeValidate from 'vee-validate';

import { LoadingState } from 'src/config/loading-state';
import Navigation from 'components/Navigation/navigation';
import Loader from 'components/Loader/loader';

Vue.use(VueRouter);
Vue.use(VeeValidate);

import 'src/config/http';
import 'src/style.scss';
import store from './store';
import router from './router';

sync(store, router);

new Vue({
  store,
  router,
  components: {
    Navigation,
    Loader
  },

  data(){
    return {
      isLoading: false
    };
  },

  created(){
    LoadingState.$on('toggle', (isLoading) => {
      this.isLoading = isLoading;
    });
  }
}).$mount('#app');
