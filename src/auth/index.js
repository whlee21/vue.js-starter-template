/**
 * Created by whlee21 on 2017. 7. 6..
 */

import Vue from 'vue';
import VueAuthenticate from 'vue-authenticate';

Vue.use(VueAuthenticate);

const vueAuth = new VueAuthenticate(Vue.http, {
  baseUrl: 'http://localhost:8080'
});

export default vueAuth;
