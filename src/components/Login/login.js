/**
 * Created by whlee21 on 17. 7. 5.
 */

import Vue from 'vue';
import template from './login.html';

export default Vue.extend({
  template,

  data() {
    return {
      email: '',
      password: '',
      formSubmitted: false
    };
  },
  methods: {
    login() {
      // this.$validator.validateAll().then(function() {
      //   this.$store.dispatch('login', {
      //     email: this.email,
      //     password: this.password
      //   }).then(() => {
      //     this.$router.push('/');
      //   });
      // }, function() { return; });
      this.$validator.validateAll();
      if (!this.errors.any()) {
        this.submitForm();
        this.$store.dispatch('login', {
          email: this.email,
          password: this.password
        }).then(() => {
          this.$router.push('/');
        });
      }
    },
    submitForm(){
      this.formSubmitted = true;
    }
  }
});
