import Vue from 'vue';
import { mapActions, mapGetters } from 'vuex';
import template from './navigation.html';

export default Vue.extend({
  template,
  methods: {
    ...mapActions(['logout'])
  },
  computed: {
    ...mapGetters(['isLoggedIn'])
  }
});
